import matplotlib.pyplot as plt
import numpy as np


def exp(value: float) -> float:
    return np.exp(value)

x = np.linspace(-3, 3, num=100)
plt.plot(x, [exp(xi) for xi in x], label="exponential")
plt.legend()
plt.show()