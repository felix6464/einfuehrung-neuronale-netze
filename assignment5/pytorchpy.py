"""
TODO
- team member 1: <name>
- team member 2: <name>

A simple training setup for a pytorch network
- every layer stores its inputs, outputs, weights, biases and gradients for weights+biases

tasks:
    - add your team members' names at the top of the file
    - implement missing code pieces marked with TODO
"""

import os
import argparse
import matplotlib.pyplot as plt
import torch
import torch.nn as nn
from torch.optim.sgd import SGD
from torch.utils.data import Dataset, DataLoader, random_split


class VectorDataset(Dataset):
    def __init__(self, data_: dict):
        self.keys = list(data_.keys())
        self.values = list(data_.values())

    def __len__(self):
        return len(self.keys)

    def __getitem__(self, idx):
        return self.keys[idx], self.values[idx]

    def get_input_output_sizes(self) -> (int, int):
        return self.keys[0].shape[0], self.values[0].shape[0]


class Network(nn.Module):
    def __init__(self, num_inputs: int, num_outputs: int, hidden_dim: int):
        super().__init__()


        # TODO design a neural network
        self.seq = nn.Sequential(
            nn.Linear(num_inputs, hidden_dim, bias=True),
            nn.ReLU(),
            nn.Linear(hidden_dim, hidden_dim, bias=True),
            nn.ReLU(),
            nn.Linear(hidden_dim, hidden_dim, bias=True),
            nn.ReLU(),
            nn.Linear(hidden_dim, hidden_dim, bias=False),
            nn.ReLU(),
            nn.Linear(hidden_dim, hidden_dim, bias=True),
            nn.ReLU(),
            nn.Linear(hidden_dim, hidden_dim, bias=True),
            nn.Sigmoid(),
            nn.Linear(hidden_dim, num_outputs, bias=False)
        )

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        # this executes all modules in self.seq in order
        # note that we do not need to implement a backwards pass ourselves!
        return self.seq(x)


def print_and_plot(training_losses: [float], test_losses: [float], plot=True):
    # print results
    print("-" * 70)
    fmt = "{:<24} {:<16} {:<16}"
    print(fmt.format("", "final value", "best value"))
    print(fmt.format("- training loss", "%.4f" % training_losses[-1], "%.4f" % min(training_losses)))
    if len(test_losses) > 0:
        print(fmt.format("- test loss", "%.4f" % test_losses[-1], "%.4f" % min(test_losses)))
    print("-" * 70)
    print(fmt.format("to compare", "final value", ""))
    ml_results = {
        "linear regression": (0.4430, "darkgreen"),
        "SVM (rbf)": (0.1197, "deeppink"),
        "xgboost (100 est)": (0.0463, "lightpink"),
        "our network": (0.0284, "turquoise"),  # common value we got while creating this assignment with a simple network
        "rand forest (50 est)": (0.0276, "lime"),
    }
    for k, (v, _) in ml_results.items():
        print(fmt.format("- %s" % k, "%.4f" % v, ""))

    # maybe plot
    if plot:
        x_ = list(range(len(training_losses)))
        plt.plot(x_, training_losses, "o--", label="training_losses")
        if len(test_losses) > 0:
            plt.plot(x_, test_losses, "o--", label="test_losses")
        for k, (v, c) in ml_results.items():
            plt.axhline(y=v, c=c, label=k)
        plt.ylim((0, max(training_losses + [ml_results["linear regression"][0]]) + 0.05))
        plt.legend()
        plt.xlabel("epochs")
        plt.ylabel("L2 loss")
        plt.show()


def iterate_loader(loader: DataLoader, cuda=False) -> (torch.Tensor, torch.Tensor):
    for k_, v_ in loader:
        if cuda:
            k_ = k_.cuda()
            v_ = v_.cuda()
        yield k_, v_


def train(train_loader, test_loader, net: nn.Module, optimizer, criterion: nn.Module, epochs: int, use_cuda: bool) -> ([], []):
    """
    train a network
    - for 'epoch' epochs
    - in each epoch, iterate both data loaders
        - as you train, note the training losses
        - after you trained for this epoch, note the test losses
        - average the batch losses for each epoch

    you can find example code here: https://pytorch.org/tutorials/beginner/basics/quickstart_tutorial.html

    :param train_loader: loader for training data
    :param test_loader: loader for test data
    :param net: network to be trained
    :param optimizer: how gradients are handled for update steps, we use stochastic gradient descent (SGD)
    :param criterion: loss function, we use mean squared error (MSE)
    :param epochs: train for this many epochs
    :param use_cuda: compute on the GPU (otherwise CPU, which is slower)
    :return: epoch-wise losses, for training and test data (of either floats or scalar tensors)
    """
    training_losses = []
    test_losses = []
    hidden = None

    for epoch in range(epochs):

        print("Epoch = ", epoch)


        for input_, target_ in iterate_loader(train_loader, cuda=use_cuda):

            net.train()
            optimizer.zero_grad()


            #print("Shape of Input : {}".format(input_.shape))
            #print("Shape of Target : {}".format(target_.shape))

            output = net.forward(input_)

            #print("Shape of Output : {}".format(output.shape))

            loss = criterion(output, target_)

            loss.backward()

            optimizer.step()

        training_losses.append(loss.detach().item())

        if epoch % 10 == 0:
            print("MSE = {}".format(loss.detach().item()))


        net.eval()

        for input_val, target_val in iterate_loader(test_loader, cuda=use_cuda):
            # No backward pass here, save errors for evalutation
            with torch.no_grad():
                outputs = net.forward(input_val)
                # print("ouputs of model: {}".format(outputs[0]))

                loss = criterion(outputs, target_val)

        test_losses.append(loss.detach().item())

        if epoch % 10 == 0:
            print("MSE_val = {}".format(loss.detach().item()))




    # return losses over the training
    assert len(training_losses) == len(test_losses) == epochs, "Need a loss for each epoch! Implement the function!"
    return training_losses, test_losses


if __name__ == '__main__':
    parser = argparse.ArgumentParser("perceptron")
    parser.add_argument('--data_file', default="%s/data.pt" % os.getcwd(), type=str)  # if the data is in the same dir as the script
    parser.add_argument('--batch_size', default=256, type=int)
    parser.add_argument('--epochs', default=100, type=int)
    parser.add_argument('--sgd_lr', default=0.01, type=float)
    parser.add_argument('--sgd_momentum', default=0.9, type=float)
    parser.add_argument('--sgd_decay', default=1e-5, type=float)
    parser.add_argument('--plot', default="true", type=str, help="plot results")
    args = parser.parse_args()
    args.plot = args.plot.lower().startswith("t")
    use_cuda_ = torch.cuda.device_count() >= 1
    torch.manual_seed(0)

    # load data, train/test split, use 2000 random samples as test set
    data = torch.load(args.data_file)
    data_set = VectorDataset(data)
    data_train, data_test = random_split(data_set, [len(data_set) - 2000, 2000])
    input_size, output_size = data_set.get_input_output_sizes()

    # create loaders
    train_loader_ = DataLoader(data_train, batch_size=args.batch_size, shuffle=True, num_workers=4)
    test_loader_ = DataLoader(data_test, batch_size=args.batch_size, shuffle=False, num_workers=4)

    # create network, optimizer, loss function
    hidden_dim = 256
    net_ = Network(input_size, output_size, hidden_dim)
    if use_cuda_:
        net_ = net_.cuda()
    optimizer_ = SGD(net_.parameters(), lr=args.sgd_lr, momentum=args.sgd_momentum, weight_decay=args.sgd_decay)
    criterion_ = nn.MSELoss()

    # train and test
    training_losses_, test_losses_ = train(train_loader_, test_loader_, net_, optimizer_, criterion_, args.epochs, use_cuda_)

    # print and plot
    print_and_plot(training_losses_, test_losses_, plot=args.plot)
