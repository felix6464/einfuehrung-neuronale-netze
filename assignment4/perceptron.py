"""
TODO
- team member 1: <name>
- team member 2: <name>

A simple numpy-based network
- every layer stores its inputs, outputs, weights, biases and gradients for weights+biases

tasks:
    - add your team members' names at the top of the file
    - implement missing code pieces marked with TODO
        - do not add code anywhere else. we run unit tests on your code, and it's on you if they fail.
        - respect the type hints. if you are supposed to return a np.array, do return one, and not a python list.
        - there is plenty of print / plot code that can help you, see the main function
        - we generally set a numpy zeros array for you as placeholder, so that you can be sure of the size,
          and that the code runs from the beginning (but of course not correctly)
        - some assert statements will further make sure that the sizes are correct after you make changes to the code
    - run the fit_network multiple times, as stated in the assignment sheet
        - add the resulting plot of each fitting curve to your submission pdf
"""

from typing import Union
import numpy as np
import matplotlib.pyplot as plt


class ActFun:
    @staticmethod
    def f(x: np.array) -> np.array:
        """ function """
        raise NotImplementedError

    @staticmethod
    def d(x: np.array) -> np.array:
        """ the first derivative """
        raise NotImplementedError


class Linear(ActFun):
    @staticmethod
    def f(x: np.array) -> np.array:
        return x

    @staticmethod
    def d(x: np.array) -> np.array:
        return np.ones_like(x)


class Sigmoid(ActFun):
    @staticmethod
    def f(x: np.array) -> np.array:
        # TODO act fun
        return x

    @staticmethod
    def d(x: np.array) -> np.array:
        # TODO derivative
        return x


class TanH(ActFun):
    @staticmethod
    def f(x: np.array) -> np.array:
        # TODO act fun
        return x

    @staticmethod
    def d(x: np.array) -> np.array:
        # TODO derivative
        return x


class Layer:
    """
    A layer in a neural network, this only serves to store values
    """

    def __init__(self, input_size: int, output_size: int, act_fun: ActFun):
        """
        :param input_size: number of inputs
        :param output_size: number of outputs
        :param act_fun: activation function
        """
        self.weights = np.random.random(size=(output_size, input_size))
        self.bias = np.zeros(shape=(output_size,))
        self.act_fun = act_fun

        # store input / inner value / output for nice printing
        self.stored_layer_input = None
        self.stored_net = None
        self.stored_out = None

        # deltas
        self.deltas = None
        self.deltas_weights = None
        self.deltas_bias = None


class Network:
    """
    stacking modules sequentially
    """

    def __init__(self, layers: [Layer]):
        super().__init__()
        self.layers = layers

    def forward(self, input_: np.array) -> np.array:
        """
        forward pass through the network
        store some info for each layer

        :param input_: network input
        :return: network output
        """
        for layer in self.layers:
            layer.stored_layer_input = input_.copy()

            # TODO these are just placeholders
            layer.stored_net = np.zeros_like(layer.bias)    # before the activation function
            layer.stored_out = np.zeros_like(layer.bias)    # after the activation function, output

            # make sure shapes are correct
            input_ = layer.stored_out
            assert input_.shape[0] == layer.weights.shape[0] == layer.bias.shape[0], "out/bias/weights do not match"
        return input_

    def backpropagation(self, targets: np.array):
        """
        go through all layers from back to front, computing their respective deltas

        :param targets: np.array of what should have been the output
        """
        # output layer delta, delta rule
        last_layer = self.layers[-1]
        last_layer.deltas = (-1) * last_layer.act_fun.d(last_layer.stored_net) * (targets - last_layer.stored_out)

        # inner layer deltas, backprop rule, compute deltas of each layer
        for layer0, layer1 in zip(reversed(self.layers[:-1]), reversed(self.layers[1:])):
            # we loop through all pairs of consecutive layers (layer0 first, layer1 following), from back to front

            # TODO this is just a placeholder. compute the delta of the current layer0
            # tip: we only have layer1.deltas, which are behind layer1 weights/biases
            layer0.deltas = np.zeros_like(layer0.stored_out)

            # make sure shapes are correct
            assert layer0.deltas.shape == layer0.stored_out.shape, "deltas' shape does not match layer's output shape!"

        # store deltas_weights/deltas_bias in each layer
        for layer in self.layers:
            # TODO these are just placeholders. compute the changes made to weights and biases
            layer.deltas_weights = np.zeros_like(layer.weights)
            layer.deltas_bias = np.zeros_like(layer.bias)

            # make sure shapes are correct
            assert layer.deltas_weights.shape == layer.weights.shape, "Weight deltas' shape does not match weights' shape!"
            assert layer.deltas_bias.shape == layer.bias.shape, "Bias deltas' shape does not match bias' shape!"

    def print(self):
        """
        print weights/input/output/gradient of every layer
        """
        def _fmt(value: Union[None, np.array]) -> str:
            if value is None:
                return ""
            return np.array_str(value, max_line_width=np.inf, precision=4).replace('\n', ',')

        print("{:<} {}".format(self.__class__.__name__, "{"))
        for i, layer in enumerate(self.layers):
            print("\t%d: %s {" % (i, self.__class__.__name__))
            print("{}{:<7}: {}".format("\t"*2, "weights", _fmt(layer.weights)))
            print("{}{:<7}: {}".format("\t"*2, "bias", _fmt(layer.bias)))
            print("{}{:<7}: {}".format("\t"*2, "input", _fmt(layer.stored_layer_input)))
            print("{}{:<7}: {}".format("\t"*2, "net", _fmt(layer.stored_net)))
            print("{}{:<7}: {}".format("\t"*2, "out", _fmt(layer.stored_out)))
            print("{}{:<7}: {}".format("\t"*2, "delta w", _fmt(layer.deltas_weights)))
            print("{}{:<7}: {}".format("\t"*2, "delta b", _fmt(layer.deltas_bias)))
            print("{}{}".format("\t", "},"))
        print("}")


class SGD:
    """
    stochastic gradient descent optimizer
    """

    def __init__(self, net: Network, learning_rate: float, weight_decay=0.0):
        self.network = net
        self.lr = learning_rate
        self.weight_decay = weight_decay

    def step(self):
        for layer in self.network.layers:
            # TODO apply deltas
            layer.weights = layer.weights
            layer.bias = layer.bias

            # TODO apply weight decay separately
            layer.weights = layer.weights
            layer.bias = layer.bias


def fit_network(target=1.0, weight_decay=0.0):
    """
    fit a network to a single input pattern
    keep the values as they are for a standardized plot
    """
    np.random.seed(0)

    v = np.array([-1, 1])
    target = np.array([target])

    network = Network([
        Layer(2, 4, Sigmoid()),
        Layer(4, 6, TanH()),
        Layer(6, 1, Linear())
    ])
    optimizer = SGD(network, learning_rate=0.05, weight_decay=weight_decay)

    values = []
    for i in range(50):
        out = network.forward(v)
        network.backpropagation(target)
        optimizer.step()
        values.append(out)
    plt.close('all')
    plt.plot(range(len(values)), values, label="output")
    plt.plot(range(len(values)), [target]*len(values), label="target")
    plt.title("target=%.1f, decay=%.3f" % (target[0], weight_decay))
    plt.xlabel("iterations")
    plt.ylabel("output and target")
    plt.legend()
    plt.show()


def forward():
    """
    play around with the code here
    - make sure all activation functions work as intended (Linear, Sigmoid, TanH)
    - for a single-layer network:
        - make sure that the output is correct
        - make sure that the output deltas are correct
        - make sure that the optimizer updates the weights correctly, so that we are getting better
    - extend to multiple layers:
        - make sure that the inner deltas are correct
        - and everything else stays correct
    """
    v = np.array([-1, 1])
    target = np.array([0.5])

    # create a network
    network = Network([
        Layer(2, 1, Sigmoid()),
    ])

    # if you want to debug with known weights, do it like this. otherwise the weights are random
    network.layers[0].weights = np.array([[1.0, 2.0]])
    network.layers[0].bias = np.array([[0.0]])

    optimizer = SGD(network, learning_rate=0.1, weight_decay=0.0)

    print('-'*100)
    print('forward')
    print('-'*100)
    network.forward(v)
    network.print()

    print('-'*100)
    print('backprop')
    print('-'*100)
    print("target", target)
    network.backpropagation(target)
    network.print()

    print('-'*100)
    print('optimizer step')
    print('-'*100)
    optimizer.step()
    network.print()


def plot_act(act_fun: ActFun):
    plt.close('all')
    x = np.linspace(-3, 3, 100)
    y = act_fun.f(x)
    yd = act_fun.d(x)
    plt.plot(x, y, label="%s f" % act_fun.__class__.__name__)
    plt.plot(x, yd, label="%s d" % act_fun.__class__.__name__)
    plt.legend()
    plt.grid()
    plt.show()


if __name__ == '__main__':
    # first implement the activation functions
    plot_act(Linear())
    plot_act(Sigmoid())
    plot_act(TanH())

    # then implement network functions
    # forward()

    # when you think your network works, train it on a single pattern:
    # fit_network(target=1.0, weight_decay=0.0)
    # fit_network(target=1.0, weight_decay=0.005)
    # fit_network(target=10.0, weight_decay=0.0)
